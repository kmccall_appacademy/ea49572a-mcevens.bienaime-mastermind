class Code

  attr_accessor :pegs
  PEGS = {"r"=> "Red", "g" => "Green","b"=> "Blue","y"=>"Yellow","o"=>"Orange","p"=>"Purple"}



  def initialize(pegs)
    @pegs = pegs
  end



  def self.parse(string)
    @parse = string
   pegs = @parse.chars.map do |letter|
     raise 'error' if PEGS[letter.downcase] == nil
     PEGS[letter]
   end
   Code.new(pegs)
  end

  def self.random
    code = PEGS.values.shuffle.take(4)
     Code.new(code)
  end

  def [](i)
      @pegs[i]
    end

  def exact_matches(code)
    matches = 0
    @pegs.each_with_index do |peg, i|
      matches += 1 if code[i] == pegs[i]
    end
    matches
  end

  def near_matches(code)
    near_matches = []

    code.pegs.each_with_index do |peg, i|
      if code[i] != pegs[i] && pegs.include?(peg)
        near_matches << peg
      end
    end
    near_matches.uniq.length
  end



  def == (other_code)
    unless other_code.instance_of? Code
     return false
   end

   unless self.exact_matches(other_code) == other_code.pegs.length
     return false
   else
     true
   end
end

end

class Game
  attr_reader :secret_code

 def initialize(secret_code = Code.random)
   @secret_code = secret_code
 end

  def get_guess
   puts "Guess a code"
   user_guess = Code.parse($stdin.gets.chomp)
  end

  def display_matches(guess)
   exact_matches = @secret_code.exact_matches(guess)
   near_matches = @secret_code.near_matches(guess)
   puts "You have #{exact_matches} exact matches!!"
   puts "You have #{near_matches} near matches!!"
   end
end
